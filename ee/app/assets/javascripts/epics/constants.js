export const status = {
  open: 'opened',
  close: 'closed',
};

export const stateEvent = {
  close: 'close',
  reopen: 'reopen',
};
